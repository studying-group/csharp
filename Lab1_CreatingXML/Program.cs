﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using static Lab1_CreatingXML.XmlBuilder;

namespace Lab1_CreatingXML
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Пересоздать xml файл? 1/0");
            if ("1" == Console.ReadLine()) {
                var xml = new XmlBuilder().XmlConstruct();
                xml.Save("./study.xml");
            }

            var doc = XDocument.Load("./study.xml");
            
            // 1
            GetSubjectsAndHours(doc);
            // 2
            GetAverageHours(doc);
            // 3
            GetGenderData(doc);
            // 4
            GetRatingsWithGroupsBySubject(doc);
            // 5
            GetGroupsWhereMaleMoreFemale(doc);
            
            Console.WriteLine("Нажмите любую клавишу чтобы выйти");
            Console.ReadKey();
        }

        // 1) предметы и часы по форме контроля
        private static void GetSubjectsAndHours(XDocument doc)
        {
            var cv = XmlBuilder.ControlsVariant;
            Console.WriteLine("Каков твой тип контроля? По умолчанию - 0");
            for (int i = 0; i < cv.Length; i++)
            {
                Console.WriteLine($"---- {i} - {cv[i]}");
            }

            var cf = int.Parse(Console.ReadLine()!);
            if (cf > cv.Length-1 || cf < 0) cf = 0;

            var queryResult =
                from subject in doc.Descendants("Дисциплина")
                where subject.Element("Контроль")?.Value == cv[cf]
                select subject;

            foreach (var subject in queryResult)
            {
                Console.WriteLine(
                    $"{subject.Element("Имя")?.Value} - {subject.Element("Часы")?.Value} ч."
                );
            }
            
            Console.WriteLine("\n");
        }
        
        // 2) Среднее количество часов по предметам
        private static void GetAverageHours(XDocument doc) {
            var averageHoursOfAll =
                from hours in doc.Descendants("Дисциплина")
                select hours.Element("Часы")?.Value;

            var hoursArray = averageHoursOfAll.Select(int.Parse).ToArray();
            Console.WriteLine($"2) Среднее количество часов по предметам: {hoursArray.Average()}");
            
            Console.WriteLine("\n");
        }
        
        // 3) Данные по студентам пола «Пол», сгруппированные по группам.
        private static void GetGenderData(XDocument doc) {
            Console.WriteLine("Введите пол: м/ж");
            var gender = Console.ReadLine()?.ToLower();
            if (gender != "м" && gender != "ж") {
                gender = "м";
            }
            
            var genders =
                from groups in doc.Descendants("Группа") 
                from students in doc.Descendants("Студент")
                where students.Element("Пол")?.Value == gender
                group students by groups.Attribute("Имя");

            foreach (var genderGroup in genders)
            {
                Console.WriteLine($"{genderGroup.Key.Value}:");
                foreach (var g in genderGroup)
                {
                    if (g.Parent?.Attribute("Имя")?.Value == genderGroup.Key.Value)
                    {
                        Console.WriteLine(g.Element("ФИО")?.Value);
                    }
                }
                Console.WriteLine("");
            }
            
            Console.WriteLine("\n");
        }
        
        // 4 Оценки студентов с указанием группы по дисц. «...» (join).
        private static void GetRatingsWithGroupsBySubject(XDocument doc)
        {
            var allSubjects = XmlBuilder.Subjects;
            Console.WriteLine("Какой предмет?");
            for (int i = 0; i < allSubjects.Length; i++)
            {
                Console.WriteLine($"---- {i} - {allSubjects[i]}");
            }
            
            var subjectCode = int.Parse(Console.ReadLine() ?? "0");
            if (subjectCode > allSubjects.Length || subjectCode < 0) subjectCode = 0;

            var queryResult =
                from rates in doc.Descendants("Оценка")
                join subjects in doc.Descendants("Дисциплина")
                    on (string) rates.Element("Предмет")
                    equals (string) subjects.Attribute("Код")
                orderby rates.Parent?.Parent?.Parent?.Attribute("Имя")?.Value, 
                    rates.Parent?.Parent?.Element("ФИО")?.Value
                where subjects.Attribute("Код")?.Value == $"sub-{subjectCode}"
                select (
                    rates.Parent?.Parent?.Parent?.Attribute("Имя")?.Value,
                    rates.Parent?.Parent?.Element("ФИО")?.Value,
                    rates.Element("Рейт")?.Value, 
                    subjects.Element("Имя")?.Value
                )
            ;

            Console.WriteLine(
                $"{"Группа", -13}  |  {"ФИО студента", -20}  |  {"Оценка", -6}  |  {"Предмет"}"
            );
            foreach (var (group, student, rate, subject) in queryResult)
            {
                Console.WriteLine(
                    $"{group, -13}  |  {student, -20}  |  {rate, -6}  |  {subject}"
                );
            }

            Console.WriteLine("\n");
        }

        // 5 группы где м < ж
        private static void GetGroupsWhereMaleMoreFemale(XDocument doc)
        {
            var groups = doc.XPathSelectElements("//Группа");
            var i = 1;
            foreach (var group in groups) {
                var male = doc.XPathSelectElements($"//Группа[{i}]/Студент[Пол='м']").Count();
                var female = doc.XPathSelectElements($"//Группа[{i++}]/Студент[Пол='ж']").Count();
                if (male < female)
                {
                    Console.WriteLine($"{group.Attribute("Имя")?.Value} - male: {male}, female: {female}");
                }
            }
        }
    }
}