using System;
using System.Collections.Generic;
using System.Xml.Linq;
using Faker;

namespace WpfApp
{
    public class XmlBuilder
    {
        // формы контроля
        public static readonly string[] ControlsVariant = new[] {
            "Зачёт",
            "Дифзач",
            "Экзамен",
            "Курсовая",
            "Накопительный"
        };

        // дисцилины
        public static readonly string[] Subjects = new[]
        {
            "Философские проблемы науки и техники",
            "Деловое сотрудничество и психология взаимодействия в коллективе",
            "Математические методы теории систем",
            "Алгоритмические языки программирования",
            "Высокопроизводительные вычисления и облачные технологии",
            "Экономический анализ автоматизированных систем",
            "Профессиональный иностранный язык"
        };
        
        
        // Создание xml файла по заданию
        public XDocument XmlConstruct() {
            // root
            var xmlDocument = new XDocument(
                new XDeclaration("1.0", "utf-8", "yes"),
                new XComment("Вариант 2: Успеваемость студентов"),
                new XElement("Успеваемость",
                    new XElement("Предметы", xml_disciplines()),
                    new XElement("Группы", xml_groups(3))
                )
            );
            return xmlDocument;
        }

        // генератор списка групп xml_groups(количество групп)
        private List<XElement> xml_groups(int groupsCount) {
            var elements = new List<XElement>();
            
            for (int i = 0; i < groupsCount; i++) {
                elements.Add(new XElement(
                        "Группа", new XAttribute("Имя", $"ГР-{Faker.NumberFaker.Number()}"),
                        
                        // студенты в группе
                        xml_students(i + 3)
                    )
                );
            }
            
            return elements;
        }
        
        // генератор списка студентов xml_students(количество студентов)
        private List<XElement> xml_students(int studentsCount) {
            var elements = new List<XElement>();
            var gender = new[] {"м", "ж"};
            
            for (int i = 0; i < studentsCount; i++) {
                var next = new Random().Next(0, gender.Length);
                elements.Add(new XElement("Студент",
                        new XElement("ФИО", $"{Faker.NameFaker.Name().ToString()}"),
                        new XElement("Пол", gender[next]),
                        new XElement("Оценки", xml_ratings())
                    )
                );

            }

            return elements;
        }
        
        // генератор xml_disciplines(int disciplinesCount, int hours)
        private List<XElement> xml_disciplines() {
            var elements = new List<XElement>();

            var code = 0;
            foreach (var subject in Subjects) {
                elements.Add(
                    new XElement(
                        "Дисциплина", 
                        new XAttribute("Код", $"sub-{code++}"),
                        new XElement("Часы", new Random().Next(36, 145)),
                        new XElement("Имя", subject),
                        new XElement("Контроль", ControlsVariant[new Random().Next(0, ControlsVariant.Length)])
                    )
                );
            }

            return elements;
        }
        
        // генератор оценок
        private List<XElement> xml_ratings()
        {
            var elements = new List<XElement>();
            var rateCount = new Random().Next(1, 10);

            for (int i = 0; i < rateCount; i++) {
                var code = new Random().Next(0, Subjects.Length);
                elements.Add(new XElement(
                        "Оценка", 
                        new XElement("Предмет", $"sub-{code}"),
                        new XElement("Рейт", new Random().Next(2, 6))
                    )
                );
            }
            
            return elements;
        }

    }
}