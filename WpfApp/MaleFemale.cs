﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp
{
    class MaleFemale
    {
        public string groupName { get; set; }
        public int maleCount { get; set; }
        public int femaleCount { get; set; }
    }
}
