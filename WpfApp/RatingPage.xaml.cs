﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml.Linq;

namespace WpfApp
{
    /// <summary>
    /// Логика взаимодействия для RatingPage.xaml
    /// </summary>
    public partial class RatingPage : Page
    {
        public XDocument doc;
        public static readonly string[] Subjects = new[]
        {
            "Философские проблемы науки и техники",
            "Деловое сотрудничество и психология взаимодействия в коллективе",
            "Математические методы теории систем",
            "Алгоритмические языки программирования",
            "Высокопроизводительные вычисления и облачные технологии",
            "Экономический анализ автоматизированных систем",
            "Профессиональный иностранный язык"
        };

        public RatingPage()
        {
            InitializeComponent();
            doc = XDocument.Load("./study.xml");
            this.comboBox.ItemsSource = Subjects;
            comboBox_SelectionChanged(null, null);
        }

        private void comboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            dataGrid1.ItemsSource = null;
            var subjectCode = this.comboBox.SelectedIndex;
            var queryResult =
                from rates in doc.Descendants("Оценка")
                join subjects in doc.Descendants("Дисциплина")
                    on (string)rates.Element("Предмет")
                    equals (string)subjects.Attribute("Код")
                orderby rates.Parent?.Parent?.Parent?.Attribute("Имя")?.Value,
                    rates.Parent?.Parent?.Element("ФИО")?.Value
                where subjects.Attribute("Код")?.Value == $"sub-{subjectCode}"
                select (
                    rates.Parent?.Parent?.Parent?.Attribute("Имя")?.Value,
                    rates.Parent?.Parent?.Element("ФИО")?.Value,
                    rates.Element("Рейт")?.Value
                )
            ;

            var studentsList = new List<Rating> { };
            foreach (var (group, student, rate) in queryResult)
            {
                studentsList.Add(new Rating { groupName = group, studentName = student, rating = rate });
            }

            dataGrid1.ItemsSource = studentsList;
        }
    }
}
