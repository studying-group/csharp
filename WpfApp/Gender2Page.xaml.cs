﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml.Linq;
using System.Xml.XPath;

namespace WpfApp
{
    /// <summary>
    /// Логика взаимодействия для Gender2Page.xaml
    /// </summary>
    public partial class Gender2Page : Page
    {
        public XDocument doc;

        public Gender2Page()
        {
            InitializeComponent();
            doc = XDocument.Load("./study.xml");
            radioButton1_Copy_Click(null, null);
        }

        private void radioButton1_Copy_Click(object sender, RoutedEventArgs e)
        {

            dataGrid2.ItemsSource = null;

            var condition = (bool)radioButton1_Copy.IsChecked;
            var groupsList = new List<MaleFemale> { };
            var groups = doc.XPathSelectElements("//Группа");
            var i = 1;
            foreach (var group in groups)
            {
                var male = doc.XPathSelectElements($"//Группа[{i}]/Студент[Пол='м']").Count();
                var female = doc.XPathSelectElements($"//Группа[{i++}]/Студент[Пол='ж']").Count();
                if (condition)
                {
                    if (male < female)
                    {
                        groupsList.Add(new MaleFemale { groupName = group.Attribute("Имя")?.Value, maleCount = male, femaleCount = female });
                    }
                }
                else
                {
                    if (male > female)
                    {
                        groupsList.Add(new MaleFemale { groupName = group.Attribute("Имя")?.Value, maleCount = male, femaleCount = female });
                    }
                }
            }

            dataGrid2.ItemsSource = groupsList;
        }
    }
}
