﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml.Linq;

namespace WpfApp
{
    /// <summary>
    /// Логика взаимодействия для GenderPage.xaml
    /// </summary>
    public partial class GenderPage : Page
    {
        public XDocument doc;

        public GenderPage()
        {
            InitializeComponent();
            doc = XDocument.Load("./study.xml");
            btnShow_Click(null, null);
        }

        private void dataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void btnShow_Click(object sender, RoutedEventArgs e)
        {
            this.dataGrid.ItemsSource = null;

            var gender = "м";
            if ((bool)this.radioButton_Copy.IsChecked)
            {
                gender = "ж";
            }

            var genders =
                from groups in doc.Descendants("Группа")
                from students in doc.Descendants("Студент")
                where students.Element("Пол")?.Value == gender
                orderby groups.Attribute("Имя")?.Value, students.Element("ФИО")?.Value
                group students by groups.Attribute("Имя");

            var studentsList = new List<GenderClass> { };

            foreach (var genderGroup in genders)
            {
                foreach (var g in genderGroup)
                {
                    if (g.Parent?.Attribute("Имя")?.Value == genderGroup.Key.Value)
                    {
                        studentsList.Add(new GenderClass { groupName = genderGroup.Key.Value, studentName = g.Element("ФИО")?.Value });
                    }
                }
            }

            this.dataGrid.ItemsSource = studentsList;
        }
    }
}
